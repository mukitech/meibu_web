package com.mukitech.model;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.aliyuncs.fc.client.FunctionComputeClient;
import com.aliyuncs.fc.request.InvokeFunctionRequest;
import com.aliyuncs.fc.response.InvokeFunctionResponse;
import com.mukitech.profile.FCProfile;
import com.mukitech.utils.MukitechOSSUtils;

public class APIModule implements Comparable<APIModule>{

	public static String TYPE_JAVA = "JAVA";
	public static String TYPE_JAVA_ADVANCED = "JAVA_ADVANCED";
	public static String TYPE_SQL = "SQL";
	public static String TYPE_GROOVY = "GROOVY";
	
	private String jsonString = "";
	private String key = "";
	private Integer no = 0;
	private Integer state = 0;
	private String method = "";
	private String output = "";
	private String comments = "";
	private String name = "";
	private String requestJSON = "";
	
	private List<SQLModule> sqls = new ArrayList<SQLModule>();
	private List<RequestModule> params = new ArrayList<RequestModule>();
	private List<RequestModule> querys = new ArrayList<RequestModule>();
	
	private JSONObject paramsObject = null;
	private JSONObject querysObject = null;
	private JSONArray sqlObject = null;
	private boolean paramOk = true;
	private String errMsg = "";
	private String type = "";
	
	
	public boolean existParamKey(String key){
		List<RequestModule> lst = getParamsAndQuerys();
		
		for( int i=0; i<lst.size(); i++){
			RequestModule item = lst.get(i);
			if( item.getName().equals( key )){
				return true;
			}
		}
		
		return false;
		
	}
	
	public String getRequestJSON() {
		return requestJSON;
	}

	public void setRequestJSON(String requestJSON) {
		this.requestJSON = requestJSON;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public boolean isParamOk() {
		return paramOk;
	}

	public void setParamOk(boolean paramOk) {
		this.paramOk = paramOk;
	}

	public JSONObject getParamsObject() {
		return paramsObject;
	}

	public void setParamsObject(JSONObject paramsObject) {
		this.paramsObject = paramsObject;
	}

	public JSONArray getSqlObject() {
		return sqlObject;
	}

	public void setSqlObject(JSONArray sqlObject) {
		this.sqlObject = sqlObject;
	}

	
	public List<RequestModule> getQuerys() {
		return querys;
	}

	public void setQuerys(List<RequestModule> querys) {
		this.querys = querys;
	}

	public JSONObject getQuerysObject() {
		return querysObject;
	}

	public void setQuerysObject(JSONObject querysObject) {
		this.querysObject = querysObject;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Integer getNo() {
		return no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

	public List<SQLModule> getSqls() {
		return sqls;
	}

	public void setSqls(List<SQLModule> sqls) {
		this.sqls = sqls;
	}

	public List<RequestModule> getParams() {
		return params;
	}

	public void setParams(List<RequestModule> params) {
		this.params = params;
	}
	
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	private String generateGetString(List<RequestModule> lst) {
		String str = "";
		for( int i=0; i<lst.size(); i++) {
			RequestModule item = lst.get(i);
			if( i != 0 ) {
				str = str + "&"+item.getName() + "=" + item.getValue();
			}else {
				str = "?"+item.getName() + "=" + item.getValue();
			}
		}
		
		return str;
	}
	
	private String generatePostString(List<RequestModule> lst) {
		JSONObject jsonObj = new JSONObject();
		
		for( int i=0; i<lst.size(); i++) {
			RequestModule item = lst.get(i);
			jsonObj.put( item.getName(), item.getValue());
		}
		
		return jsonObj.toString();
	}
	
	
	public List<RequestModule> getParamsAndQuerys() {
		List<RequestModule> lst = new ArrayList<RequestModule>();
		
		lst.addAll( params );
		lst.addAll( querys );
		
		return lst;
	}
	
	public RequestModule getParamsAndQuerysModule( String name ) {
		List<RequestModule> lst = getParamsAndQuerys();
		
		for( int i=0;i<lst.size();i++) {
			RequestModule module = lst.get(i);
			if( module.getName().equals( name )) {
				return module;
			}
		}
		
		return null;
	}
	
	
	public String getParamsAndQuerysModuleValue( String name ) {
		RequestModule module = this.getParamsAndQuerysModule(name);
		if( module == null) {
			return null;
		}
		
		return module.getValue();
	}
	
	
	public String getShowParams() {
		
		List<RequestModule> lst = new ArrayList<RequestModule>();
		
		lst.addAll( params );
		lst.addAll( querys );
			
		if( this.getMethod().equals("GET")) {
			return this.generateGetString(lst);
		}else {
			return this.generatePostString(lst);
		}
		
	}
	
	private JSONObject getParamJSONObject() {
		JSONObject jsonObject = new JSONObject();
		for( int i=0;i<params.size();i++) {
			RequestModule item = params.get(i);
			jsonObject.put( item.getName(), item.getValue());
		}
		return jsonObject;
	}
	
	private JSONObject getQuerysJSONObject() {
		JSONObject jsonObject = new JSONObject();
		for( int i=0;i<querys.size();i++) {
			RequestModule item = querys.get(i);
			jsonObject.put( item.getName(), item.getValue());
		}
		return jsonObject;
	}

	private JSONArray getSQLJSONArray() {
		JSONArray jsonArray = new JSONArray();
		for( int i=0;i<sqls.size();i++) {
			SQLModule item = sqls.get(i);
			
			
			JSONObject obj = new JSONObject();
			
			obj.put("sql", item.getSql());
			obj.put("type", item.getType());
			obj.put("output_name", item.getOutName());
			
			jsonArray.put( obj );
		}
		
		return jsonArray;
	}
	public static APIModule fromJSON(String key, String jsonString) {
		
		APIModule module = new APIModule();

		try {
			JSONObject obj = new JSONObject(jsonString);
			
			Integer no = obj.getInt("no");
			String method = obj.getString("method");
			String type = APIModule.TYPE_SQL;
			
			try {
				type = obj.getString("type").trim();
			}catch(Exception e) {
				type = APIModule.TYPE_SQL;
			}
			

			try {
				key = obj.getString("key").trim();
			}catch(Exception e) {
				
			}
			
			String output = obj.getString("output");
			String comments = obj.getString("comments");
			String name = obj.getString("name");
			
			module.setKey(key);
			module.setJsonString(jsonString);
			
			module.setNo(no);
			module.setMethod(method);
			module.setOutput(output);
			module.setComments(comments);
			module.setName(name);
			module.setType(type);
			
			JSONObject jsonParams = obj.getJSONObject("params");
			JSONObject jsonQuerys = obj.getJSONObject("querys");

			JSONArray jsonSqls = obj.getJSONArray("sqls");
			
			module.paramsObject = jsonParams;
			module.sqlObject = jsonSqls;
			module.querysObject = jsonQuerys;
			
			//SQL解析
			for( int i=0; i<jsonSqls.length();i++) {
				JSONObject sqlObject = jsonSqls.getJSONObject(i);
				String sql = sqlObject.getString("sql");
				String output_name = sqlObject.getString("output_name");
				String sql_type = sqlObject.getString("type");
				
				SQLModule sqlModule = new SQLModule();
				sqlModule.setOutName(output_name);
				sqlModule.setSql(sql);
				sqlModule.setType(sql_type);
				
				module.getSqls().add( sqlModule);
			}
			
			//JSON解析
			
			if( module.getType().equals(APIModule.TYPE_GROOVY) ||
				module.getType().equals(APIModule.TYPE_JAVA) ||
				module.getType().equals(APIModule.TYPE_SQL)	) {
				for( String itemName : jsonParams.keySet() ) {
					String demoValue = jsonParams.getString( itemName);
					module.getParams().add( new RequestModule( itemName,demoValue));
				}
				
				for( String itemName : jsonQuerys.keySet() ) {
					String demoValue = jsonQuerys.getString( itemName);
					module.getQuerys().add( new RequestModule( itemName,demoValue));
				}
			}else {
				//高级JAVA
				//不做处理
			}
			
			module.setState( 1);
			
		}catch(Exception e) {
			module.setState( 0 );
			
			System.out.println( e.getMessage());
		}
		
		return module;
	}

	public int compareTo(APIModule o) {
		return o.no - this.no;
	}
	
	public String toJSON() {
		JSONObject obj = new JSONObject(jsonString);
		
		if( this.getType().equals(APIModule.TYPE_JAVA_ADVANCED)) {
			//如果是高级的JAVA调用，那么Param不做解析
			JSONObject jsonObject = new JSONObject( this.getRequestJSON());
			
			obj.put("params", jsonObject );
			obj.put("key", this.getKey());
		}else {
			obj.put("params", getParamJSONObject());
			obj.put("querys", getQuerysJSONObject());
			obj.put("sqls", getSQLJSONArray());
			obj.put("key", this.getKey());
		}
		return obj.toString();
	}
	
	
	public String invoke() {
		
			 String accessKey = FCProfile.accessKeyId;
		     String accessSecretKey = FCProfile.accessKeySecret;
		     String accountId = FCProfile.accountId;
		     
		     String FUNCTION_NAME = "second_fat";
		     
			 FunctionComputeClient fcClient = new FunctionComputeClient(FCProfile.REGION, accountId, accessKey, accessSecretKey);
			 InvokeFunctionRequest invkReq = new InvokeFunctionRequest(FCProfile.SERVICE_NAME, FUNCTION_NAME);
			
			 try {
				invkReq.setPayload(this.toJSON().getBytes("UTF-8"));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				return e.getMessage();
			}
			 
		    InvokeFunctionResponse invkResp = fcClient.invokeFunction(invkReq);
		    String content = new String(invkResp.getContent(),Charset.forName("UTF-8"));
		    
		    return content;
		
	}
	
	
	public static void main(String[] args) {
		String key = "manager_cloth_detial";
		String jsonString = MukitechOSSUtils.getApiModuleString( key );
		APIModule module = APIModule.fromJSON(key, jsonString);
		
		System.out.println( module.invoke());
	}
	
}
