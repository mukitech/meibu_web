package com.mukitech.model;

public class SQLModule {
	
	public final static  String SELECT = "SELECT";
	public static  String UPDATE = "UPDATE";
	public static  String INSERT = "INSERT";
	public static  String DELETE = "DELETE";
	public static  String SAVE_OR_UPDATE = "SAVE_OR_UPDATE";
	public static  String EXIST = "EXIST";

	private String outName;
	private String sql;
	private String type;
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOutName() {
		return outName;
	}
	public void setOutName(String outName) {
		this.outName = outName;
	}
	public String getSql() {
		return sql;
	}
	public void setSql(String sql) {
		this.sql = sql;
	}
	
	
	
}
