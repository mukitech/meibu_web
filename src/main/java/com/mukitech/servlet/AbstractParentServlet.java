package com.mukitech.servlet;

import java.io.IOException;
import java.nio.charset.Charset;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.aliyuncs.fc.client.FunctionComputeClient;
import com.aliyuncs.fc.request.InvokeFunctionRequest;
import com.aliyuncs.fc.response.InvokeFunctionResponse;
import com.mukitech.model.APIModule;
import com.mukitech.profile.FCProfile;
import com.mukitech.utils.LanguageUtils;
import com.mukitech.utils.MukitechOSSUtils;
import com.mukitech.utils.URLUtils;

public abstract class AbstractParentServlet extends HttpServlet{

	public abstract String getProfix();
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setHeader("Access-Control-Allow-Origin","*");

		String uri = request.getRequestURI();
		
		String module = URLUtils.getModuleFromUrl(uri);
		
		response.setContentType("text/json;charset=UTF-8;pageEncoding=UTF-8");

		 String accessKey = FCProfile.accessKeyId;
	     String accessSecretKey = FCProfile.accessKeySecret;
	     String accountId = FCProfile.accountId;
	     
	     String FUNCTION_NAME = "second_fat";
	     
	        // Initialize FC client
	     FunctionComputeClient fcClient = new FunctionComputeClient(FCProfile.REGION, accountId, accessKey, accessSecretKey);

		 InvokeFunctionRequest invkReq = new InvokeFunctionRequest(FCProfile.SERVICE_NAME, FUNCTION_NAME);
	      String payload = MukitechOSSUtils.getApiModuleString( module );
	      
	      //获取模块
	      APIModule apiModule = APIModule.fromJSON( module, payload);
	      //填充参数
	      URLUtils.dealwithRequest( apiModule, request );
	      
	      if( apiModule.getType().equals(APIModule.TYPE_GROOVY) ) {
	    	  
		      if( apiModule.isParamOk()) {
		    	  
			      invkReq.setPayload(apiModule.toJSON().getBytes("UTF-8"));
			      InvokeFunctionResponse invkResp = fcClient.invokeFunction(invkReq);
			      String content = new String(invkResp.getContent(),Charset.forName("UTF-8"));
			      content = LanguageUtils.filter(content, getProfix());
			      response.setCharacterEncoding("UTF-8");
			      response.getWriter().write(content);
		      }else {
		    	  JSONObject obj = new JSONObject();
		    	  obj.put("result", "failure");
		    	  obj.put("msg", apiModule.getErrMsg());
		    	  
		    	  response.setCharacterEncoding("UTF-8");
			      response.getWriter().write(obj.toString());
		      }
	      }else if( apiModule.getType().equals(APIModule.TYPE_SQL) ) {
	    	  
		      if( apiModule.isParamOk()) {
		    	  
			      invkReq.setPayload(apiModule.toJSON().getBytes("UTF-8"));
			      InvokeFunctionResponse invkResp = fcClient.invokeFunction(invkReq);
			      String content = new String(invkResp.getContent(),Charset.forName("UTF-8"));
			      content = LanguageUtils.filter(content, getProfix());
			      response.setCharacterEncoding("UTF-8");
			      response.getWriter().write(content);
		      }else {
		    	  JSONObject obj = new JSONObject();
		    	  obj.put("result", "failure");
		    	  obj.put("msg", apiModule.getErrMsg());
		    	  
		    	  response.setCharacterEncoding("UTF-8");
			      response.getWriter().write(obj.toString());
		      }
	      }else if( apiModule.getType().equals(APIModule.TYPE_JAVA) ) {
	    	  //用JAVA实现的Module
	    	  	    	  	
	    	  if( apiModule.isParamOk()) {
		    	  
			      invkReq.setPayload(apiModule.toJSON().getBytes("UTF-8"));
			      InvokeFunctionResponse invkResp = fcClient.invokeFunction(invkReq);
			      String content = new String(invkResp.getContent(),Charset.forName("UTF-8"));
			      
			      //如果服务器还没有实现这个函数，那么
			      if (content.equals("underdevelopment")) {
			    	  //String demoKey = "demo/" + module;
			    	  
				      //String demoResponse = MukitechOSSUtils.getDemoContent(  demoKey );
				      
			    	  JSONObject obj = new JSONObject();
			    	  obj.put("result", "failure");
			    	  obj.put("msg", "这个接口还没有实现呢!!!!!,加油!!!");
				      response.setCharacterEncoding("UTF-8");
			    	  response.getWriter().write( obj.toString());
			    	  
			    	  return;
			      }else {
			    	  response.setCharacterEncoding("UTF-8");
				      content = LanguageUtils.filter(content, getProfix());
			    	  response.getWriter().write(content);
			    	  
			    	  return;
			      }
		      }else {
		    	  JSONObject obj = new JSONObject();
		    	  obj.put("result", "failure");
		    	  obj.put("msg", apiModule.getErrMsg());
		    	  
		    	  response.setCharacterEncoding("UTF-8");
			      response.getWriter().write(obj.toString());
			      
			      return;
		      }
	    	  
	      }else if( apiModule.getType().equals(APIModule.TYPE_JAVA_ADVANCED) ) {
	    	  //用JAVA实现的Module
	    	  	    	  	
	    	  	  String json = apiModule.toJSON();
	    	  	  
	    	  	  MukitechOSSUtils.saveRequestContent( apiModule.getKey(), json );
	    	  	  
			      invkReq.setPayload(json.getBytes("UTF-8"));
			      InvokeFunctionResponse invkResp = fcClient.invokeFunction(invkReq);
			      String content = new String(invkResp.getContent(),Charset.forName("UTF-8"));
			      
			      //如果服务器还没有实现这个函数，那么
			      if (content.equals("underdevelopment")) {
			    	  String demoKey = "demo/" + module;
			    	  
				      String demoResponse = MukitechOSSUtils.getDemoContent(  demoKey );
				      
				      response.setCharacterEncoding("UTF-8");
			    	  response.getWriter().write( demoResponse);
			    	  
			      }else {
			    	  response.setCharacterEncoding("UTF-8");
				      content = LanguageUtils.filter(content, getProfix());
			    	  response.getWriter().write(content);
			      }
		      
	      }else {
	    	  JSONObject obj = new JSONObject();
	    	  obj.put("result", "failure");
	    	  obj.put("msg", apiModule.getKey() + "未知错误");
	    	  
	    	  response.setCharacterEncoding("UTF-8");
		      response.getWriter().write(obj.toString());
	      }

	}
	
	public static void main(String[] args) {

		 String accessKey = FCProfile.accessKeyId;
	     String accessSecretKey = FCProfile.accessKeySecret;
	     String accountId = FCProfile.accountId;
	     
	     String FUNCTION_NAME = "second_fat";
	     
	        // Initialize FC client
	     FunctionComputeClient fcClient = new FunctionComputeClient(FCProfile.REGION, accountId, accessKey, accessSecretKey);

		 InvokeFunctionRequest invkReq = new InvokeFunctionRequest(FCProfile.SERVICE_NAME, FUNCTION_NAME);
	      String payload = MukitechOSSUtils.getApiModuleString("java_interface_demo");

	      APIModule module = APIModule.fromJSON("java_interface_demo", payload);
	      invkReq.setPayload(module.getJsonString().getBytes());
	      InvokeFunctionResponse invkResp = fcClient.invokeFunction(invkReq);
	      String content = new String(invkResp.getContent(),Charset.forName("UTF-8"));
	      
	      System.out.println( module.getType() );

	      content = LanguageUtils.filter(content,"_cn");
	      System.out.println( content );

	      
	}
	
}
