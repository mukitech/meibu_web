package com.mukitech.servlet;
import java.io.*;
import java.util.List;

import javax.servlet.*;    
import javax.servlet.http.*;

import com.mukitech.model.APIModule;
import com.mukitech.utils.MukitechOSSUtils;


public class FaceServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html;charset=UTF-8;pageEncoding=UTF-8");
	
		List<APIModule> apiList = MukitechOSSUtils.getApiModules();
		
		request.setAttribute("apilist", apiList);
		String jspPath = "/WEB-INF/jsp/face.jsp";
		RequestDispatcher dispatcher=request.getRequestDispatcher(jspPath);
		
		dispatcher.forward(request,response);

	}
	        
}
