package com.mukitech.servlet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mukitech.model.APIModule;
import com.mukitech.profile.OSSProfile;
import com.mukitech.utils.MukitechOSSUtils;
import com.mukitech.utils.URLUtils;

public class OSSServlet  extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Access-Control-Allow-Origin","*");

		String uri = request.getRequestURI();
		
		String module = URLUtils.getModuleFromUrl(uri);
		
		response.setContentType("text/json;charset=UTF-8;pageEncoding=UTF-8");
		if( module.equals("clearlog")) {
			
			MukitechOSSUtils.clearLogs();
			
			String content = "{\"result\":\"success\"}";
			
			response.setContentType("text/text;charset=UTF-8;pageEncoding=UTF-8");
	    	 response.setCharacterEncoding("UTF-8");
		     response.getWriter().write(content.toString());
			
		}else if( module.equals("log")) {
			response.setContentType("text/html;charset=UTF-8;pageEncoding=UTF-8");

			String jspPath = "/WEB-INF/jsp/log.jsp";
			
			RequestDispatcher dispatcher=request.getRequestDispatcher(jspPath);
			request.setAttribute("logs",  MukitechOSSUtils.getLogs());
			
			dispatcher.forward(request,response);
			
		}else if( module.equals("view")) {
		
			String jspPath = "/WEB-INF/jsp/form.jsp";
			RequestDispatcher dispatcher=request.getRequestDispatcher(jspPath);
				
			dispatcher.forward(request,response);
			
		}else if( module.equals("load")) {
			String name = request.getParameter("name");
			String content = MukitechOSSUtils.getApiModuleString(name);
			
	    	 response.setCharacterEncoding("UTF-8");
		     response.getWriter().write(content.toString());
			
		}else if( module.equals("delete")) {
			String name = request.getParameter("name");
			String content = MukitechOSSUtils.deleteModule(name);
			
			response.setContentType("text/text;charset=UTF-8;pageEncoding=UTF-8");
	    	 response.setCharacterEncoding("UTF-8");
		     response.getWriter().write(content.toString());
			
		}else if( module.equals("load_groovy")) {
			String name = request.getParameter("name");
			String content = MukitechOSSUtils.getGroovyString(name);
			
			response.setContentType("text/text;charset=UTF-8;pageEncoding=UTF-8");
	    	 response.setCharacterEncoding("UTF-8");
		     response.getWriter().write(content.toString());
			
		}else if( module.equals("update")) {
			String name = request.getParameter("name");
			String content = request.getParameter("content");
			
			boolean result = MukitechOSSUtils.update(name, content);
			
			JSONObject obj = new JSONObject();
	    	 
			if( result == true ) {
				obj.put("result", "success");
				obj.put("msg", "保存成功");
			 
			}else {
				   obj.put("result", "failure");
		    	    obj.put("msg", "该接口已经存在，请重新命名吧");
			}
	    	  response.setCharacterEncoding("UTF-8");
		      response.getWriter().write(obj.toString());
		}else if(module.equals("update_groovy") ){
			String name = request.getParameter("name");
			String content = request.getParameter("content");
			
			boolean result = MukitechOSSUtils.update_groovy(name, content);
			
			JSONObject obj = new JSONObject();
	    	 
			if( result == true ) {
				obj.put("result", "success");
				obj.put("msg", "保存成功");
			}else {
				   obj.put("result", "failure");
		    	   obj.put("msg", "该接口已经存在，请重新命名吧");
			}
	    	response.setCharacterEncoding("UTF-8");
		    response.getWriter().write(obj.toString());
		    
		}else if( module.equals("save")) {
			String name = request.getParameter("name");
			String content = request.getParameter("content");
			
			boolean result = MukitechOSSUtils.save(name, content);
			 
			JSONObject obj = new JSONObject();
	    	 
			if( result == true ) {
				obj.put("result", "success");
				obj.put("msg", "保存成功");
			
			}else {
				 obj.put("result", "failure");
		    	 obj.put("msg", "该接口已经存在，请重新命名吧");	    
			}
			
	    	response.setCharacterEncoding("UTF-8");
		    response.getWriter().write(obj.toString());
		      
		}else if( module.equals("save_groovy")) {
			String name = request.getParameter("name");
			String content = request.getParameter("content");
			
			boolean result = MukitechOSSUtils.save_groovy(name, content);
			 
			JSONObject obj = new JSONObject();
	    	 
			if( result == true ) {
				obj.put("result", "success");
				obj.put("msg", "保存成功");
			
			}else {
				 obj.put("result", "failure");
		    	 obj.put("msg", "该接口已经存在，请重新命名吧");	    
			}
			
	    	  response.setCharacterEncoding("UTF-8");
		      response.getWriter().write(obj.toString());
		      
		}else if( module.equals("test")) {
			String name = request.getParameter("name");
			String content = request.getParameter("content");
			
			APIModule apiModule = APIModule.fromJSON(name, content );
			
			String outputString = apiModule.invoke();
			
			response.setCharacterEncoding("UTF-8");
		    response.getWriter().write(outputString.toString());
		}else if( module.equals("upload")) {
			
			String content;
			
			try {
				content = this.uploadImage(request, response);
				
				response.setCharacterEncoding("UTF-8");
			    response.getWriter().write(content.toString());
			    
			} catch (FileUploadException e) {
				
				JSONObject obj = new JSONObject();
				
				 obj.put("result", "failure");
		    	 obj.put("msg", e.getMessage());	
		    	 
				
		    	 response.setCharacterEncoding("UTF-8");
				 response.getWriter().write(obj.toString());
				    
			}
			
			
		}
	}
	
	
	public String uploadImage( HttpServletRequest request, HttpServletResponse response ) throws FileUploadException, IOException {
	      
        JSONObject obj = new JSONObject();

		obj.put("result", "success");
        obj.put("msg", "上传图片成功");

        JSONArray data = new JSONArray();
        
	        DiskFileItemFactory factory = new DiskFileItemFactory();  
	        ServletFileUpload sfu = new ServletFileUpload(factory);  
	        sfu.setFileSizeMax( 20* 1024 * 1024);  
	  
	        FileItemIterator items = sfu.getItemIterator(request);  
	          
	        // 判断路径是否存在，不存在则创建目录  
	        String contextPathString = request.getSession().getServletContext()  
	                .getRealPath("/");  
	        contextPathString += "/images/userImages";  
	        File parentfile = new File(contextPathString);  
	        if (!parentfile.exists() && !parentfile.isDirectory()) {  
	            if (!parentfile.mkdir())  
	                System.out.println("目录创建失败");  
	        }  
	  
	        Random rdRandom = new Random(100000);  
	        String randNameString = null;  
	        FileItemStream fileItemStream = null;  
	        String filename = null;  
	        String suffixName = null;  
	        String fieldName = null;  
	        InputStream inputStream = null;  

	        int index = 0;
	        
	        while (items.hasNext()) {  
	            
	        	fileItemStream = items.next();  
	            
	            try {  
	                inputStream = fileItemStream.openStream();  
	            } catch (Exception e1) {  
	                // TODO Auto-generated catch block  
	                System.out.println("图片太大了！");  
	                
	            } 
	            
	            //简单的不是图片的接口
	            if (fileItemStream.isFormField()) {  
	                fieldName = new String(fileItemStream.getFieldName().getBytes(  
	                        "ISO-8859-1"), "utf-8");  
	                continue;  
	            }  
	          
	            fieldName = new String(fileItemStream.getFieldName().getBytes(  
	                    "ISO-8859-1"), "utf-8");  
	            filename = fileItemStream.getName(); // 获取文件名
	            
	            //处理文件名
	            if (filename != null && !filename.equals(""))  {
	                suffixName = filename.substring(filename.lastIndexOf("."));
	            }else {  
	                continue;
	            }
	  
	            Date NowTime = new Date();
	            DateFormat formatter = new SimpleDateFormat ("yyyyMMddHHmmss"); 

	            randNameString = formatter.format(NowTime)  
	                    + rdRandom.nextInt();  
	  
	            filename = randNameString + fieldName + suffixName;
	            
	            MukitechOSSUtils.saveRequestImage(filename, inputStream);
	            
	            data.put(OSSProfile.demoUrlPrefix +filename );
	            
	            index++;
	        }
	        
	    obj.put("data", data );
		return obj.toString();

	}

}
