package com.mukitech.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class LanguageUtils {

	public static String filter(String content,String profix ) {
	
		if( content.startsWith("[")) {
			JSONArray jsonArray = new JSONArray(content);
			jsonArray = filterArray(jsonArray, profix);
			
			return jsonArray.toString();
		}else {
			JSONObject jsonObject = new JSONObject(content);
			
			jsonObject = filterObject(jsonObject, profix);
			
			return jsonObject.toString();
		}
		
	}
	
	public static JSONArray filterArray(JSONArray jsonArray,String profix) {
		for( int i=0; i<jsonArray.length(); i++ ) {
			Object obj = jsonArray.get(i);
			
			if( obj.getClass().getName().equals("org.json.JSONArray")) {
	        	 filterArray((JSONArray)obj, profix);
	        }else if( obj.getClass().getName().equals("org.json.JSONObject")){
	        	 JSONObject newObj = filterObject((JSONObject)obj, profix);
	        	 //jsonArray.remove(i);
	        	// jsonArray.put(i, newObj);
	        }
			
		}
		
		return jsonArray;
	}
	
	public static JSONObject filterObject(JSONObject jsonObject,String profix) {
		 Iterator iterator = jsonObject.keys();
		 
		 Map<String,String> map = new HashMap<String,String>();
		 
		 while( iterator.hasNext()){
		         String key = (String) iterator.next();
		         Object obj = jsonObject.get(key);
		         
		         System.out.println( obj.getClass().getName());
		         
		         if( obj.getClass().getName().equals("org.json.JSONArray")) {
		        	 JSONArray newJSONArray = filterArray((JSONArray)obj, profix );
		        	 jsonObject.put(key, newJSONArray );
		        }else if( obj.getClass().getName().equals("org.json.JSONObject")){
		        	 JSONObject newObj = filterObject((JSONObject)obj, profix);
		        	 jsonObject.put(key, newObj );
		        	 
		        }else if( obj.getClass().getName().equals("java.lang.String")) {
		        	
		        	if( key.endsWith( profix )) {
		        		String newKey = key.replace( profix, "");
		        		String value = (String)obj;
		        		map.put( newKey, value);
		        	}
		        	
		        }
		         
		 }
		 
		 
		 for (String key : map.keySet()) {
			 	String value = map.get(key);
			 	jsonObject.remove( key +"_cn");
			 	jsonObject.remove( key +"_en");
			    jsonObject.put( key, value);
		 }
		 
		 return jsonObject;
	}
	
}
