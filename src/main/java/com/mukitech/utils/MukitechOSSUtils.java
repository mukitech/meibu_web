package com.mukitech.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.ListObjectsRequest;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.OSSObjectSummary;
import com.aliyun.oss.model.ObjectListing;
import com.aliyun.oss.model.PutBucketImageRequest;
import com.mukitech.model.APIModule;
import com.mukitech.model.LogItem;
import com.mukitech.profile.OSSProfile;

public class MukitechOSSUtils {
	

	
	private static List<String> getAllApiNames(){
		
		List<String> lstApiNames = new ArrayList<String>();
		
		OSSClient ossClient = new OSSClient(
				OSSProfile.endpoint, 
				OSSProfile.accessKeyId, 
				OSSProfile.accessKeySecret);
		
		ObjectListing lst = ossClient.listObjects(OSSProfile.bucketName);
		
		List<OSSObjectSummary> lstSummary = lst.getObjectSummaries();
		
		for (OSSObjectSummary summary : lstSummary) {
		    String key = summary.getKey();
		    lstApiNames.add( key  );
		}
		
		ossClient.shutdown();
		
		return lstApiNames;
	}
	
	public static List<APIModule> getApiModules(){
		
		List<APIModule> lstApiModules = new ArrayList<APIModule>();
		
		List<String> lstNames = getAllApiNames();
		
		for( String key:lstNames ){
			String jsonString = getApiModuleString( key ); 
			APIModule module = APIModule.fromJSON( key, jsonString );
			lstApiModules.add(module);
		}
		
		Collections.sort(lstApiModules);
		
		return lstApiModules;
	}
	
	
	public static void saveRequestContent(String key,String saveContent) {
		OSSClient ossClient = new OSSClient(
				OSSProfile.endpoint, 
				OSSProfile.accessKeyId, 
				OSSProfile.accessKeySecret);
		
		try {
			byte[] content = saveContent.getBytes("UTF-8");
			ossClient.putObject( OSSProfile.bucketDemoName, "request/" + key, new ByteArrayInputStream(content));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		ossClient.shutdown();

	}
	
	public static void saveRequestImage( String name, InputStream inputStream) {
		OSSClient ossClient = new OSSClient(
				OSSProfile.endpoint, 
				OSSProfile.accessKeyId, 
				OSSProfile.accessKeySecret);
		ossClient.putObject( OSSProfile.bucketDemoName , "upload_images/" +name , inputStream );
		ossClient.shutdown();
	}
	
	
	public static String getDemoContent(String key ) {
		OSSClient ossClient = new OSSClient(
				OSSProfile.endpoint, 
				OSSProfile.accessKeyId, 
				OSSProfile.accessKeySecret);
		
		OSSObject ossObject = ossClient.getObject(OSSProfile.bucketDemoName, key );
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(ossObject.getObjectContent(), Charset.forName("UTF-8")));
		StringBuffer sb = new StringBuffer();
		
		boolean isEnd = false;
		
		while (isEnd == false) {
			
			try {
				String line= reader.readLine();
			    if (line == null) {
			    	isEnd = true;
			    	break;
			    }
				sb.append(line);
				sb.append("\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ossClient.shutdown();
		String jsonString = sb.toString();
		
		return jsonString;
	}
	
	public static String getApiModuleString(String key) {

		OSSClient ossClient = new OSSClient(
				OSSProfile.endpoint, 
				OSSProfile.accessKeyId, 
				OSSProfile.accessKeySecret);
		
		OSSObject ossObject = ossClient.getObject(OSSProfile.bucketName, key );
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(ossObject.getObjectContent(), Charset.forName("UTF-8")));
		StringBuffer sb = new StringBuffer();
		
		boolean isEnd = false;
		
		while (isEnd == false) {
			
			try {
				String line= reader.readLine();
			    if (line == null) {
			    	isEnd = true;
			    	break;
			    }
				sb.append(line);
				sb.append("\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ossClient.shutdown();
		String jsonString = sb.toString();
		
		return jsonString;
		
	}
	
	
	public static String getGroovyString(String key) {

		OSSClient ossClient = new OSSClient(
				OSSProfile.endpoint, 
				OSSProfile.accessKeyId, 
				OSSProfile.accessKeySecret);
		
		key = key + ".groovy";

		boolean exist = ossClient.doesObjectExist(OSSProfile.bucketGroovyName, key);
		
		if( exist == false ) {
			return "";
		}
		
		OSSObject ossObject = ossClient.getObject(OSSProfile.bucketGroovyName, key );
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(ossObject.getObjectContent(), Charset.forName("UTF-8")));
		StringBuffer sb = new StringBuffer();
		
		boolean isEnd = false;
		
		while (isEnd == false) {
			
			try {
				String line= reader.readLine();
			    if (line == null) {
			    	isEnd = true;
			    	break;
			    }
				sb.append(line);
				sb.append("\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ossClient.shutdown();
		String jsonString = sb.toString();
		
		return jsonString;
		
	}
	
	
	public static boolean save(String key,String saveContent ) {
		
		key = key.trim();
		saveContent = saveContent.trim();
		
		OSSClient ossClient = new OSSClient(
				OSSProfile.endpoint, 
				OSSProfile.accessKeyId, 
				OSSProfile.accessKeySecret);
		
		List<String> apiNames = getAllApiNames();
		
		if( apiNames.contains( key )) {
			return false;
		}
		
		try {
			byte[] content = saveContent.getBytes("UTF-8");
			ossClient.putObject( OSSProfile.bucketName, key, new ByteArrayInputStream(content));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return true;
	}
	
	
	public static boolean save_groovy(String key,String saveContent ) {
		
		key = key.trim();
		saveContent = saveContent.trim();
		
		OSSClient ossClient = new OSSClient(
				OSSProfile.endpoint, 
				OSSProfile.accessKeyId, 
				OSSProfile.accessKeySecret);
		
		List<String> apiNames = getAllApiNames();
		
		if( apiNames.contains( key )) {
			return false;
		}
		
		key = key + ".groovy";
		
		try {
			byte[] content = saveContent.getBytes("UTF-8");
			ossClient.putObject( OSSProfile.bucketGroovyName, key , new ByteArrayInputStream(content));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return true;
	}
	
	
	public static boolean update_groovy(String key,String saveContent) {
		
		key = key.trim();
		saveContent = saveContent.trim();
		
		OSSClient ossClient = new OSSClient(
				OSSProfile.endpoint, 
				OSSProfile.accessKeyId, 
				OSSProfile.accessKeySecret);
		
		key = key + ".groovy";

		try {
			byte[] content = saveContent.getBytes("UTF-8");
			ossClient.putObject( OSSProfile.bucketGroovyName, key, new ByteArrayInputStream(content));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return true;
	}
	
	public static boolean update(String key,String saveContent) {
		
		key = key.trim();
		saveContent = saveContent.trim();
		
		
		OSSClient ossClient = new OSSClient(
				OSSProfile.endpoint, 
				OSSProfile.accessKeyId, 
				OSSProfile.accessKeySecret);
		
		try {
			byte[] content = saveContent.getBytes("UTF-8");
			ossClient.putObject( OSSProfile.bucketName, key, new ByteArrayInputStream(content));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return true;
	}

	public static void main(String[] args) {
		String storeKey = "TEST.json";
		String content = "齐学智就是我了" + (new Date()).toLocaleString();
		
		boolean saveResult = update( storeKey, content );
		
		System.out.println("saveresult:" + saveResult);
	}

	
	public static String deleteModule(String key) {
		key = key.trim();
		
		OSSClient ossClient = new OSSClient(
				OSSProfile.endpoint, 
				OSSProfile.accessKeyId, 
				OSSProfile.accessKeySecret);
		
		ossClient.deleteObject(OSSProfile.bucketName, key);

		return "删除成功了 【 " + key + "】";
	}
	
	
private static List<String> getAllLogs(){
		
		List<String> lstApiNames = new ArrayList<String>();
		
		OSSClient ossClient = new OSSClient(
				OSSProfile.endpoint, 
				OSSProfile.accessKeyId, 
				OSSProfile.accessKeySecret);
		
		ListObjectsRequest listObjectsRequest = new ListObjectsRequest();
		
		listObjectsRequest.setBucketName(OSSProfile.bucketDemoName);
		listObjectsRequest.setMaxKeys(999);
		
		ObjectListing lst = ossClient.listObjects(listObjectsRequest);
		
		List<OSSObjectSummary> lstSummary = lst.getObjectSummaries();
		
		for (OSSObjectSummary summary : lstSummary) {
		    String key = summary.getKey();
		    if( key.startsWith("logs/2")) {
		    	lstApiNames.add( key  );
		    }
		}
		
		ossClient.shutdown();
		
		return lstApiNames;
	}

	public static void deleteLogContent(String key ) {
		key = key.trim();
		
		OSSClient ossClient = new OSSClient(
				OSSProfile.endpoint, 
				OSSProfile.accessKeyId, 
				OSSProfile.accessKeySecret);
		
		ossClient.deleteObject(OSSProfile.bucketDemoName, key);

	}

	public static void clearLogs() {
		List<String> lst =getAllLogs();
		
		ArrayList<LogItem> items = new ArrayList<LogItem>();
		
		for( int i=0; i<lst.size(); i++) {
			String key = lst.get(i);
			deleteLogContent( key );
		}
		
	}

	public static String getLogString(String key) {

		OSSClient ossClient = new OSSClient(
				OSSProfile.endpoint, 
				OSSProfile.accessKeyId, 
				OSSProfile.accessKeySecret);

		
		OSSObject ossObject = ossClient.getObject(OSSProfile.bucketDemoName, key );
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(ossObject.getObjectContent(), Charset.forName("UTF-8")));
		StringBuffer sb = new StringBuffer();
		
		boolean isEnd = false;
		
		while (isEnd == false) {
			
			try {
				String line= reader.readLine();
			    if (line == null) {
			    	isEnd = true;
			    	break;
			    }
				sb.append(line);
				sb.append("\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ossClient.shutdown();
		String jsonString = sb.toString();
		
		return jsonString;
		
	}
	
	public static ArrayList<LogItem> getLogs() {
		List<String> lst =getAllLogs();
		
		ArrayList<LogItem> items = new ArrayList<LogItem>();
		
		for( int i=0; i<lst.size(); i++) {
			String key = lst.get(i);
			String content = getLogString( key );
			
			LogItem item = new LogItem();
			item.key = key;
			item.content = content;
			
			items.add( item );
		}
		
		return items;
	}
}
