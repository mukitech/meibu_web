package com.mukitech.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import com.mukitech.model.APIModule;
import com.mukitech.model.RequestModule;

public class URLUtils {

	public static String getModuleFromUrl(String url) {
		
		if( url.endsWith("/")) {
			int lastIndex = url.lastIndexOf("/");
			url = url.substring(0, lastIndex);
		}
		
		int startIndex = url.lastIndexOf("/")+1;
		
		String module = url.substring(startIndex);
		
		return module;
	}
	
	public static Map<String,String> getDemoMaps(String demoParams, String demoValues){
		HashMap<String,String> map = new HashMap<String,String>();
		
		String[] params = demoParams.split(",");
		String[] values = demoValues.split(",");
		
		if( params.length > values.length) {
			for( int i=0; i<values.length; i++) {
				map.put( params[i], values[i]);
			}
		}else{
			for( int i=0; i<params.length; i++) {
				map.put( params[i], values[i]);
			}
		}
		return map;
	}
	
	
	public static String getPostJson(String demoParams, String demoValues) {
		
		Map<String,String> map = getDemoMaps(demoParams,demoValues);
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("{");

		for (Map.Entry<String, String> entry : map.entrySet()) {  
			sb.append("\"" + entry.getKey() +"\":\"" +entry.getValue() +"\",\n");
		}  
		
		sb.append("\"format\":\"true\"");
		
		sb.append("}");
		
		return sb.toString();
	}
	
	public static String getGetUrl(String demoParams, String demoValues) {
		Map<String,String> map = getDemoMaps(demoParams,demoValues);
		
		StringBuffer sb = new StringBuffer();
		boolean isFirst = true;
		for (Map.Entry<String, String> entry : map.entrySet()) {  
			  
			if( isFirst == true) {
				sb.append("?" + entry.getKey() +"=" +entry.getValue());
				isFirst = false;
			}else {
				sb.append("&" + entry.getKey() +"=" + entry.getValue() );
			}
		}  
		
		return sb.toString();
	}
	
	public static String getFineInterfaceURL( String requestURL, String module) {
		
		int startIndex = requestURL.indexOf("/meibu_web/");
		
		String url = requestURL.substring(0, startIndex);
		url = url + "/meibu_web/general/" + module;
		
		return url;
		
	}
	
	
	public static String getRelativeURL( String requestURL, String module) {
		
		int startIndex = requestURL.indexOf("/meibu_web/");
		
		String url = requestURL.substring(0, startIndex);
		url = url + "/meibu_web/" + module;
		
		return url;
		
	}

	
	
	public static void dealWithRequestMapJava(APIModule apiModule, Map<String,String> map) {
		
		List<RequestModule> returnParams = new ArrayList<RequestModule>();
		List<RequestModule> returnQuerys = new ArrayList<RequestModule>();
		
		List<RequestModule> params = apiModule.getParams();
		List<RequestModule> querys = apiModule.getQuerys();

		for( int i=0; i<params.size(); i++) {
			RequestModule item = params.get(i);
			String name = item.getName();
			String realValue = map.get( name );
			
			if( realValue == null ) {
				
			}else {
				item.setValue( realValue );
				returnParams.add( item );
			}
		}
		
		for( int i=0; i<querys.size(); i++) {
			RequestModule item = querys.get(i);
			String name = item.getName();
			String realValue = map.get( name );
			
			if( realValue == null ) {
				
			}else {
				item.setValue( realValue );
				returnQuerys.add( item );
			}
		}
		
		apiModule.setQuerys( returnQuerys);
		apiModule.setParams( returnParams);
		
	}
	
	   /**      
     * 描述:获取 post 请求的 byte[] 数组
     * <pre>
     * 举例：
     * </pre>
     * @param request
     * @return
     * @throws IOException      
     */
    public static byte[] getRequestPostBytes(HttpServletRequest request)
            throws IOException {
        int contentLength = request.getContentLength();
        if(contentLength<0){
            return null;
        }
        byte buffer[] = new byte[contentLength];
        for (int i = 0; i < contentLength;) {

            int readlen = request.getInputStream().read(buffer, i,
                    contentLength - i);
            if (readlen == -1) {
                break;
            }
            i += readlen;
        }
        return buffer;
    }

    /**      
     * 描述:获取 post 请求内容
     * <pre>
     * 举例：
     * </pre>
     * @param request
     * @return
     * @throws IOException      
     */
    public static String getRequestPostStr(HttpServletRequest request)
            throws IOException {
    	BufferedReader br = new BufferedReader(new InputStreamReader((ServletInputStream) request.getInputStream(), "utf-8"));
    	StringBuffer sb = new StringBuffer("");
    	String temp;
    	while ((temp = br.readLine()) != null) { 
    	  sb.append(temp);
    	}
    	br.close();
    	
    	String params = sb.toString();

        return params;
    }
	
	
    public static void dealWithRequestMapAdvancedJava(APIModule apiModule, HttpServletRequest request) {
	
    	String requestJSON = "";
		
		try {
			requestJSON = getRequestPostStr(request);
			
  	  	  MukitechOSSUtils.saveRequestContent( apiModule.getKey() + "xxxx", requestJSON );

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		apiModule.setRequestJSON(requestJSON);
	}
	
	public static void dealWithRequestMapSQL(APIModule apiModule, Map<String,String> map) {

		List<RequestModule> params = apiModule.getParams();
		List<RequestModule> querys = apiModule.getQuerys();

		for( int i=0; i<params.size(); i++) {
			RequestModule item = params.get(i);
			String name = item.getName();
			String realValue = map.get( name );
			
			if( realValue == null ) {
				apiModule.setErrMsg( apiModule.getErrMsg() + " 参数：" + name + "不能为空 | ");
				apiModule.setParamOk(false);
			}else {
				item.setValue( realValue );
			}
		}
		
		for( int i=0; i<querys.size(); i++) {
			RequestModule item = querys.get(i);
			String name = item.getName();
			String realValue = map.get( name );
			
			if( realValue == null ) {
				//apiModule.setErrMsg( apiModule.getErrMsg() + " 参数：" + name + "不能为空 | ");
				//apiModule.setParamOk(false);
			}else {
				item.setValue( realValue );
			}
		}
	}
	
	public static void dealwithRequest(APIModule apiModule, HttpServletRequest request) {
		Map<String,String> map = new HashMap<String,String>();
		
		Enumeration enu=request.getParameterNames();  

		while(enu.hasMoreElements()){  
			String paraName=(String)enu.nextElement();  
			String value = request.getParameter(paraName);  
			map.put( paraName, value);
		}

		if( apiModule.getType().equals( APIModule.TYPE_SQL)) {
			dealWithRequestMapSQL( apiModule, map);
		}else if( apiModule.getType().equals(APIModule.TYPE_JAVA)) {
			dealWithRequestMapJava( apiModule, map);
		}else if( apiModule.getType().equals(APIModule.TYPE_GROOVY)) {
			dealWithRequestMapJava( apiModule, map);
		}else if( apiModule.getType().equals(APIModule.TYPE_JAVA_ADVANCED)) {
			dealWithRequestMapAdvancedJava( apiModule, request);
		}
		
		for (String key : map.keySet()) { 
			if( !apiModule.existParamKey( key )){
				String value = map.get( key );
				RequestModule item = new RequestModule(key, value);
				
				apiModule.getParams().add( item );
			}
		} 
	
	}
	
	
	
}
