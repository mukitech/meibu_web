<%@ page import="java.util.*" %><%@ page import="com.mukitech.utils.*" %><%@ page import="com.mukitech.model.*" %><%@ page import="java.util.ArrayList" %><%@ page import="javax.servlet.http.*" %><%@ page import="javax.servlet.*" %><%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>接口页面</title>
<link href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap-theme.css" rel="stylesheet">
    <link href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet">
     <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="//cdn.bootcss.com/jquery/3.1.1/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
		padding:10px;
	}
	
	#preview{
	position:absolute;
	border:1px solid #ccc;
	background:#333;
	padding:5px;
	display:none;
	color:#fff;
	}
	</style>

</head>
<body>
<div id="container">
<h1>JAVA 云函数动态化接口平台</h1>
<span>作者：齐学智，2017年10月16日</span>
<hr/>
<%
List<APIModule> lst = (List<APIModule>)request.getAttribute("apilist");

%>
<a href="#null" class="btn btn-success" onclick="showInterface()"> + 新增接口 </a> &nbsp;&nbsp;
<a href="#null" class="btn btn-danger" onclick="refreshPage()"> & 刷新页面 </a>
<a href="/meibu_web/oss/log" class="btn btn-warning" target="_blank"> 日志 </a>

<div style="display:none" id="interface">
<iframe src="/meibu_web/oss/view" style="width:100%;height:750px;border-width:1px;border-color:green;border-style:dashed;margin:5px;"></iframe>
</div>
<br/>


<table class="table table-striped">
	<thead>
		<tr>
			<td>序号</td>
			<td>接口名字</td>
			<td>请求方式</td>
			<td>请求参数</td>
			<td width="400px">实现语句</td>
		</tr>
	</thead>		
	<tbody>
<%		
int index = 0;

for( APIModule module:lst){
	index++;
%>
		<tr>
			<td nowrap>
				<%=module.getNo() %> . <%=module.getKey() %> <br/><br/>
				<hr/>
					<span class="label label-success">
						<%=module.getType() %>
					</span>
				<hr/>
				
				<a href="/meibu_web/oss/view?name=<%=module.getKey() %>" class="btn btn-success" target="_blank">编辑 </a>
				<br/><br/>
				<a href="#null" onclick="delete_interface('/meibu_web/oss/delete?name=<%=module.getKey() %>')" class="btn btn-danger">删除</a>
			</td>
			<td nowrap>
				<span style='font-color:blue;'><%=module.getName()%></span>
				<br/>
				<hr/>
				
				<a href="http://meibu-interface-images.oss-cn-hangzhou.aliyuncs.com/<%=module.getKey()%>.png?x-oss-process=style/preview" target="_blank" class="preview"><img src="http://meibu-interface-images.oss-cn-hangzhou.aliyuncs.com/<%=module.getKey()%>.png?x-oss-process=style/thumb" alt="gallery thumbnail"  width="200" height="300"></a>
				
				<hr/>
				<% String api_url = URLUtils.getFineInterfaceURL(request.getRequestURL().toString(), module.getKey()); %>
				
				<% if( module.getMethod().equals("GET")) {%>
				
				<a href="#null" onclick='load_request("<%=api_url+module.getShowParams()%>",<%=index%>)' class="btn btn-primary">加载</a>
				<a href="#null" onclick='close_div(<%=index%>)' class='btn btn-danger'>X关闭</a>
				<%}else{ %>
				 
				<br/>
				<%
				if( module.getType().equals(APIModule.TYPE_JAVA_ADVANCED)){
				%>	
				<script>
					var obj_<%=index%> = <%=module.getParamsObject().toString()%>;
				</script>

					<a href="#null" onclick='post_raw_request("<%=api_url %>",<%=index%>,obj_<%=index%>)' class="btn btn-primary">加载</a>
				<%	
				}else{
				%>
					<script>
					var obj_<%=index%> = <%=module.getShowParams()%>;
					</script>
					<br/>
					
					<a href="#null" onclick='post_request("<%=api_url %>",<%=index%>,obj_<%=index%>)' class="btn btn-primary">加载</a>
					
				<%	
				}
				%>
				
				
				
				<a href="#null" onclick='close_div(<%=index%>)' class='btn btn-danger'>X关闭</a>
				<%	
				}
				%>
				
				<br/>
				
				<% if( module.getMethod().equals("GET")) {%>
					<a href="<%=api_url+module.getShowParams()%>"><%=api_url+module.getShowParams()%></a>
				<%	
				}else{
				%>
	
				<%=api_url %>
	
				<% } %>
			</td>
			
			
			<td><span><%=module.getMethod()%></span></td>
			<td>
				<pre><%=module.getComments()%></pre>
				<hr/>
				必填：params：<pre><%=module.getParamsObject().toString().replace(",",",\n")%></pre>
				选填：querys:<pre><%=module.getQuerysObject().toString().replace(",",",\n")%></pre>
			</td>
			
			<td>
				<div style="background:#999999;color:#000000;width:400;height:400px;overflow:scroll;">
					<%=module.getJsonString().replace(" ","&nbsp;").replace("\n","<br/>")%>;
				</div>
			</td>
		</tr>
		<tr>
			<td></td>
			<td colspan="5">
				<pre id="DIV_<%=index%>">----点击加载内容-----</pre>
			</td>
		</tr>
<% } %>
	
	</tbody>	
</table>


<div style="border-style:solid;border-width:1px;border-color:green;margin:10px;padding:10px;">
<span class="label label-success">上传图片(5M以内)</span>
<br/>
<b>接口地址：</b>
<br/>
<%=URLUtils.getRelativeURL(request.getRequestURL().toString(),("oss/upload"))%>
<br/>
<b>请求方法：</b><br/>
POST



<form enctype="multipart/form-data" method="post" action="/meibu_web/oss/upload">

图片1：<input type="file" name="image_1"/><br/>
图片2：<input type="file" name="image_2"/><br/>
图片3：<input type="file" name="image_3"/><br/><hr/>

<input type="submit" value="提交"/><br/>

</form>

</div>

</div>
</body>
</html>



<script language="javascript">


function imagePreview(){	
		
		xOffset = 10;
		yOffset = 30;

		$("a.preview").hover(function(e){
		this.t = this.title;
		this.title = "";	
		var c = (this.t != "") ? "<br/>" + this.t : "";
		$("body").append("<p id='preview'><img src='"+ this.href +"' alt='Image preview' />"+ c +"</p>");								 
		$("#preview")
			.css("top",(e.pageY - xOffset) + "px")
			.css("left",(e.pageX + yOffset) + "px")
			.fadeIn("fast");						
    },
	function(){
		this.title = this.t;	
		$("#preview").remove();
    });	
	$("a.preview").mousemove(function(e){
		$("#preview")
			.css("top",(e.pageY - xOffset) + "px")
			.css("left",(e.pageX + yOffset) + "px");
	});			
};


// starting the script on page load
$(document).ready(function(){
	imagePreview();
});

function showInterface(){
   $("#interface").show();
}

function refreshPage(){
   document.location.reload();
}

function close_div( div_id){
	$("#DIV_"+div_id).html( "" );
	
}

function load_request( url, div_id){
	$.get( url, function( data ) {
		var text =  JSON.stringify(data,null,4);
	 	$("#DIV_"+div_id).html(text);
	});
	
}

function post_raw_request( url, div_id, data_submit){

	$.ajax({ 
		type: "POST", 
		url: url, 
		contentType: "application/json; charset=utf-8", 
		data: JSON.stringify(data_submit), 
		dataType: "json", 
		success: function (data) { 
			var text =  JSON.stringify(data,null,4);
		 	$("#DIV_"+div_id).html(text);	
			}, 
		error: function (data) { 
			var text =  JSON.stringify(data,null,4);
		 	$("#DIV_"+div_id).html(text);
			} 
		}); 

}


function post_request( url, div_id, data_submit){

	 $.post( url, data_submit ,function(result){
		 	var text = JSON.stringify(result,null,4);
		 	$("#DIV_"+div_id).html(text);
		  });
}


function delete_interface(url ){
	  var r=confirm("真的要删除掉该接口吗？慎重！")
	  var data_submit = {};
	  
	  if (r==true)
	    {
		  $.post( url, data_submit ,function(result){
				alert( result );
				window.location.reload();
			});
	    }
	
}


</script>