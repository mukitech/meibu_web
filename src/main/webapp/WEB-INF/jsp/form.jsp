<%@ page import="java.util.*" %><%@ page import="com.mukitech.utils.*" %><%@ page import="com.mukitech.model.*" %><%@ page import="java.util.ArrayList" %><%@ page import="javax.servlet.http.*" %><%@ page import="javax.servlet.*" %><%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>接口页面</title>
<link href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap-theme.css" rel="stylesheet">
    <link href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet">
     <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="//cdn.bootcss.com/jquery/3.1.1/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://www.promisejs.org/polyfills/promise-6.1.0.js"></script>
  <script type="text/javascript" src="http://gosspublic.alicdn.com/aliyun-oss-sdk.min.js"></script>
  <script type="text/javascript" src="/static/app_meibu.js"></script>
  <link rel="stylesheet" href="/static/style.css" />
	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
		padding:10px;
	}
	</style>

</head>
   <script src="/static/node_modules/monaco-editor/min/vs/loader.js"></script>
    <script src="/static/node_modules/jquery/dist/jquery.min.js"></script>
<script>
        var monacoEditor;
        //init monaco
        require.config({ paths: { 'vs': '/static/node_modules/monaco-editor/min/vs' } });
      
        function ReCreateEditor( jsonString) {
            $("#content").children().remove();
            require(['vs/editor/editor.main'], function () {
                monacoEditor = monaco.editor.create(document.getElementById('content'), {
                    value: jsonString,
                    language: 'json',
                    wordWrap: "off",   //自动换行，注意大小写
                    wrappingIndent: "indent"
                });
            });
        }
        function GetEditorValue() {
            return monacoEditor.getValue();
        }
    </script>


<script>
        var monacoEditorGroovy;
        
        function GroovyReCreateEditor( text) {
            $("#groovy").children().remove();
            require(['vs/editor/editor.main'], function () {
            	monacoEditorGroovy = monaco.editor.create(document.getElementById('groovy'), {
                    value: text,
                    language: 'python',
                    wordWrap: "off",   //自动换行，注意大小写
                    wrappingIndent: "indent"
                });
            });
        }
        
        function GetGroovyEditorValue() {
            return monacoEditorGroovy.getValue();
        }
    </script>


<body>
<div id="container">

<% String name= request.getParameter("name"); %>

<%
if( name == null){

String save_api_url = "/meibu_web/oss/save"; 

%>
<h1> + 新建接口</h1>
<form action="<%=save_api_url%>" method="post">
	接口名：<br/>
	<input type="text" value="<%=name%>" name="name" id="name"/> <br/>
	接口内容：<br/>
	<table class="table table-bordered">
	<tr><td>
		<span class="label label-success">接口定义：</span>
	<hr/>
		<div id="content" style="width:400px;height:500px;border-width:1px;border-color:blue;"></div>
	<hr/>
		<input type="button" value="保存" onclick="save()" class="btn btn-primary"/><br/>
	<br/>
	</td>
	
	<td>
		<span class="label label-success">执行结果：</span>
		<hr/>
			<pre id="result" style="margin:10px;padding:5px;border-style:solid; border-width:1px;border-color:blue;width:600px;height:500px;"></pre>
		<hr/>
		<input type="button" value="执行" onclick="test()" class="btn btn-primary"/>
			
	</td>
	
	<td>
	<span class="label label-success">GROOVY代码：</span>
	<hr/>
		<div id="groovy" style="width:1000px;height:500px;border-width:1px;border-color:blue;"></div>
	<hr/>
	<input type="button" value="保存GROOVY" onclick="save_groovy()" class="btn btn-primary"/><br/>
	
	</td>
	
	
	</tr>
	</table>
	
</form>
<script>
$(document).ready(function(){
	
	var url = "/meibu_web/oss/load?ran="+Math.random()+"&name=TEMPLATE.json";
	load_init_request( url, "content");
	
	GroovyReCreateEditor("\n\n\n\n\n\n\n\n\n\n\n\n");
});
</script>
<% 



}
else
{
	String update_api_url = "/meibu_web/oss/update"; 

%>

<form action="<%=update_api_url%>" method="post">
	接口名：<br/>
	<input type="text" value="<%=name%>" name="name" id="name"/> <br/>
	接口内容：<br/>
	<table class="table table-bordered">
	<tr><td>
		<span class="label label-success">接口定义：</span>
	<hr/>
		<div id="content" style="width:400px;height:500px;border-width:1px;border-color:blue;"></div>
	<hr/>
		<input type="button" value="保存" onclick="update()" class="btn btn-primary"/><br/>
	<br/>
	</td>
	
	<td>
		<span class="label label-success">执行结果：</span>
		<hr/>
			<pre id="result" style="margin:10px;padding:5px;border-style:solid; border-width:1px;border-color:blue;width:600px;height:500px;"></pre>
		<hr/>
		<input type="button" value="执行" onclick="test()" class="btn btn-primary"/>
			
	</td>
	
	<td>
	<span class="label label-success">GROOVY代码：</span>
	<hr/>
		<div id="groovy" style="width:1000px;height:500px;border-width:1px;border-color:blue;"></div>
	<hr/>
	<input type="button" value="保存GROOVY" onclick="update_groovy()" class="btn btn-primary"/><br/>
	
	</td>
	
	
	</tr>
	</table>
	
</form>

<script>
$(document).ready(function(){
	var url = "/meibu_web/oss/load?ran="+Math.random()+"&name="+"<%=name%>";
	load_init_request( url, "content");
	
	var url = "/meibu_web/oss/load_groovy?ran="+Math.random()+"&name="+"<%=name%>";
	load_init_request_groovy( url, "groovy");
	
	//GroovyReCreateEditor("\n\n\n\n\n\n\n\n\n\n\n\n");
});

</script>
<%} %>

 
</div>

  <table>
      <tr>
        <td>
          <div class="panel panel-primary">
            <div class="panel-heading">Upload file</div>
            <div class="panel-body">
              <form action="" class="form-horizontal">
                <div class="form-group">
                  <label>Select file</label>
                  <input type="file" id="file" />
                </div>
                <div class="form-group">
                  <label>Store as</label>
                  <input type="text" class="form-control" id="object-key-file" value="<%=name%>.png" />
                </div>
                <div class="form-group">
                  <input type="button" class="btn btn-primary" id="file-button" value="Upload" />
                </div>
              </form>
              <br />
              <div class="progress">
                <div id="progress-bar"
                     class="progress-bar"
                     role="progressbar"
                     aria-valuenow="0"
                     aria-valuemin="0"
                     aria-valuemax="100" style="min-width: 2em;">
                  0%
                </div>
              </div>
            </div>
          </div>
        </td>
      </tr>
     
    </table>
    
    
</body>
</html>

    

<script language="javascript">


function close_div( div_id){
	$("#DIV_"+div_id).html( "" );
	
}

function load_request( url, div_id){
	$.getJSON( url, function( data ) {
		var text = JSON.stringify( data, null, 4);
	 	$("#"+div_id).text(text);
	});
	
}


function load_init_request( url, div_id){
	$.getJSON( url, function( data ) {
		var text = JSON.stringify( data, null, 4);
		ReCreateEditor(text);
	});
}

function load_init_request_groovy(url, div_id){
	$.get( url, function( data ) {
		var text = data;
		GroovyReCreateEditor(text);
	});
}

function test(){
	
	
	if( checkJSON() == false){
		return;
	}

	
		var data_submit = {};
		data_submit["name"] = "<%=name%>";
		data_submit["content"] = GetEditorValue();
		
		var url = "/meibu_web/oss/test";
		$.post( url, data_submit ,function(result){
		 	var text = JSON.stringify(result,null,4);
		 	$("#result").html(text);
		  });
}

function update(){
	
	if( checkJSON() == false){
		return;
	}

	
		var data_submit = {};
		data_submit["name"] = "<%=name%>";
		data_submit["content"] = GetEditorValue();
		
		var url = "/meibu_web/oss/update";
		
		$("#result").html("开始保存......");
		
		$.post( url, data_submit ,function(result){
		 	var text = JSON.stringify(result,null,4);
		 	$("#result").html("保存接口定义\n" + text);
		  });
}


function update_groovy(){
	var data_submit = {};
	data_submit["name"] = "<%=name%>";
	data_submit["content"] = GetGroovyEditorValue();
	
	var url = "/meibu_web/oss/update_groovy";
	
	$("#result").html("开始保存......");
	
	$.post( url, data_submit ,function(result){
	 	var text = JSON.stringify(result,null,4);
	 	$("#result").html("保存GROOVY\n" + text );
	  });
}

function checkJSON(){
	var content = GetEditorValue();
	try{
		var json = JSON.parse(content);
	}catch(err){
		alert(err);
		return false;
	}	
	return true;
}


function save(){
	
		if( checkJSON() == false){
			return;
		}

		var data_submit = {};
		data_submit["name"] = $("#name").val();
		if( $("#name").val() == ""){
			alert("名字不能为空");
			return;
		}
		data_submit["content"] = GetEditorValue();
		
		var url = "/meibu_web/oss/save";
		$.post( url, data_submit ,function(result){
		 	var text = JSON.stringify(result,null,4);
		 	$("#result").html(text);
		  });
}


function save_groovy(){
	
	var data_submit = {};
	data_submit["name"] = $("#name").val();
	
	if( $("#name").val() == ""){
		alert("名字不能为空");
		return;
	}
	
	data_submit["content"] = GetGroovyEditorValue();
	
	var url = "/meibu_web/oss/save_groovy";
	
	$.post( url, data_submit ,function(result){
	 	var text = JSON.stringify(result,null,4);
	 	$("#result").html(text);
	  });
}

function post_request( url, div_id, data_submit){

	data_submit["format"] = true;
	
	 $.post( url, data_submit ,function(result){
		 	var text = JSON.stringify(result,null,4);
		 	$("#DIV_"+div_id).html(text);
		  });
}


</script>