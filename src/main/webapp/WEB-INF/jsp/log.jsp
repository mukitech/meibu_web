<%@ page import="java.util.*" %><%@ page import="com.mukitech.utils.*" %><%@ page import="com.mukitech.model.*" %><%@ page import="java.util.ArrayList" %><%@ page import="javax.servlet.http.*" %><%@ page import="javax.servlet.*" %><%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>日志</title>
<link href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap-theme.css" rel="stylesheet">
    <link href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet">
     <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="//cdn.bootcss.com/jquery/3.1.1/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
		padding:10px;
	}
	
	#preview{
	position:absolute;
	border:1px solid #ccc;
	background:#333;
	padding:5px;
	display:none;
	color:#fff;
	}
	</style>

</head>
<body>
<div id="container">
<h1>访问记录</h1>
<span>作者：齐学智，2017年12月01日</span>
<hr/>
<%
List<LogItem> lst = (List<LogItem>)request.getAttribute("logs");
%>

<a href="/yimei_web/oss/clearlog" class="btn btn-warning" target="_blank"> 清除日志 </a>

<table class="table table-striped">
	<thead>
		<tr>
			<td>序号</td>
			<td>内容</td>
		</tr>
	</thead>		
	<tbody>
<%		
int index = 0;

for( LogItem item:lst){
	index++;
%>
		<tr>
			<td><%=item.key%></td>
			<td><%=item.content%></td>
		</tr>	
<% } %>
	
	</tbody>	
</table>

</div>
</body>
</html>

